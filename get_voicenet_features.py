from kaldi_io import read_mat_scp
from keras.models import load_model
import keras.backend as K
import sys
import numpy as np


#Load the feature file with labels
# feat_scp_file=sys.argv[1]
model_file=sys.argv[2]
# function that parses scp file to give feats and labels
def scp_parser(feat_scp_file, maxlen=250, speaker_id_pick=3):
    # speake
    # maxlen = 250  # 5 seconds length frame shift 20ms
    spks_ = []
    mfcc = []

    for key, mat in read_mat_scp(open(feat_scp_file, 'r')):
        spk_id = key.split('-')[-1]
        if (len(spk_id)>3 and '_and_' not in spk_id
            and 'unknown' not in spk_id and 'crowd' not in spk_id):

            if mat.shape[0] < maxlen:
                mat = np.pad(mat, ((0, maxlen - mat.shape[0]), (0, 0)), 'constant', constant_values=0)
            else:
                l = (mat.shape[0] - maxlen) / 2
                mat = mat[l:l + maxlen, :]

            mfcc.append(mat)
            spks_.append(spk_id)

    mfcc = np.array(mfcc)

    spks = spks_
    nb_classes = len(set(spks))
    _, spks_1hot = np.unique(spks, return_inverse=True)
    labels = np.eye(nb_classes)[spks_1hot]
    return np.array(mfcc), spks, labels

feat_scp_file = './feats/all_utts_mfcc.scp'
mfcc, spks, labels = scp_parser(feat_scp_file)



model=load_model(model_file)



first_dense_layer=K.function([model.layers[0].input,K.learning_phase()],[model.layers[-4].output])

out_feats=first_dense_layer([mfcc,0])[0]   #testing phase

np.savez(sys.argv[3],key=spks,feats=out_feats)
