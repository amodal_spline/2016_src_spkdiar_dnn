from kaldi_io import read_mat_scp
from keras.layers.core import Dense, Dropout, Activation
from keras.models import load_model
import keras.backend as K
import sys
import numpy as np


#Load the feature file ith labels
feat_scp_file=sys.argv[1]
model_file=sys.argv[2]


batch_size=50
n_epochs=100
mfcc=[]
spks=[]
maxlen=250	#5 seconds length frame shift 20ms

for key,mat in read_mat_scp(open(feat_scp_file,'r')):
	if mat.shape[0]<maxlen:
		mat=np.pad(mat,((0,maxlen-mat.shape[0]),(0,0)),'constant',constant_values=0)
	else:
		l=(mat.shape[0]-maxlen)/2
		mat=mat[l:l+maxlen,:]

		if mat.shape[1]>0:
			mfcc.append(mat)
			#spks.append(key.split('_')[1]) #for reddots
			spks.append(key[:3])   #useful for wsj or aurora
		else:
			print mat.shape

mfcc=np.array(mfcc)
print mfcc.shape

nb_classes=len(set(spks))
_,spks=np.unique(spks,return_inverse=True)
labels=np.eye(nb_classes)[spks]

model=load_model(model_file)

model.pop()
model.pop()
model.pop()
model.add(Dense(nb_classes,name='dense_classifier'))
model.add(Activation('softmax',name='activation_softmax'))

for layer in model.layers[:-2]:
	layer.trainable=False

model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
model.fit(mfcc,labels, batch_size=batch_size, nb_epoch=n_epochs,validation_split=0.2)
