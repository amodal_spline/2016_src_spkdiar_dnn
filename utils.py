from collections import Counter
from kaldi_io import read_mat_scp
from scipy.misc import comb
import sys
import numpy as np
import os
import itertools
import copy
# function that parses scp file to give feats and labels
def scp_parser(feat_scp_file, maxlen=250, speaker_id_pick=3):
    # speake
    # maxlen = 250  # 5 seconds length frame shift 20ms
    spks_ = []
    mfcc = []

    for key, mat in read_mat_scp(open(feat_scp_file, 'r')):
        if mat.shape[0] < maxlen:
            mat = np.pad(mat, ((0, maxlen - mat.shape[0]), (0, 0)), 'constant', constant_values=0)
        else:
            l = (mat.shape[0] - maxlen) / 2
            mat = mat[l:l + maxlen, :]

        mfcc.append(mat)
        # pick speaer ID - change this!!
        spks_.append(key.split('-')[-1])

    mfcc = np.array(mfcc)
    # put in your speaker seg line here
    spks = [i[:speaker_id_pick] for i in spks_]
    nb_classes = len(set(spks))
    _, spks_1hot = np.unique(spks, return_inverse=True)
    labels = np.eye(nb_classes)[spks_1hot]
    return mfcc, spks, labels
