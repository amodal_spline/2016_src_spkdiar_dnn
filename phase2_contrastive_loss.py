#@ Krishna Somandepalli - Amodal Spline
from kaldi_io import read_mat_scp
from keras.models import Sequential, Model
from keras.layers.core import Dense, Dropout, Activation
from keras.layers import  Input, Lambda, Merge, Reshape, merge
from keras.layers.recurrent import LSTM
from keras.layers.convolutional import AtrousConvolution1D
from keras.layers.pooling import MaxPooling1D
from keras.models import load_model
import keras.backend as K
from collections import Counter
from scipy.misc import comb
import sys
import numpy as np
import os
import itertools
import copy
from keras.optimizers import RMSprop, SGD, Nadam
from random import shuffle
from keras.constraints import nonneg, maxnorm


np.random.seed(343)

def random_combination(iterable, r):
    "Random selection from itertools.combinations(iterable, r)"
    pool = tuple(iterable)
    indices = np.random.choice(len(pool), r, replace=False)
    return [pool[i] for i in indices]


# function that parses scp file to give feats and labels
def scp_parser(feat_scp_file, maxlen=250, speaker_id_pick=3):
    # speake
    # maxlen = 250  # 5 seconds length frame shift 20ms
    spks_ = []
    mfcc = []

    for key, mat in read_mat_scp(open(feat_scp_file, 'r')):
        if mat.shape[0] < maxlen:
            mat = np.pad(mat, ((0, maxlen - mat.shape[0]), (0, 0)), 'constant', constant_values=0)
        else:
            l = (mat.shape[0] - maxlen) / 2
            mat = mat[l:l + maxlen, :]

        mfcc.append(mat)
        spks_.append(key.split('-')[0])

    mfcc = np.array(mfcc)

    spks = [i[:speaker_id_pick] for i in spks_]
    nb_classes = len(set(spks))
    _, spks_1hot = np.unique(spks, return_inverse=True)
    labels = np.eye(nb_classes)[spks_1hot]
    return mfcc, spks, labels


def create_pos_neg_pairs(tr_spk_idxs):
    # input is a list of lists of data indices subject wise
    tr_spk_idxs_ = copy.deepcopy(tr_spk_idxs)
    neg_pairs = []
    # start with one list of idxs - pick one randomly and then remove it and then
    # keep doing that for subsequent lists as well
    for tr_spk_i, tr_spk_idx in enumerate(tr_spk_idxs_):
        if len(tr_spk_idx) > 0:
            neg_anchor_idx = np.random.choice(len(tr_spk_idx), 1)
            neg_anchor = tr_spk_idx[neg_anchor_idx]
            tr_spk_idx.pop(neg_anchor_idx)
            # now pick 2 from all others
            for i_ in range(len(tr_spk_idxs_)):
                if i_ != tr_spk_i & len(tr_spk_idxs_[i_]) > 0:
                        # if len(tr_spk_idxs_[i_]) > 0:
                        rand_choice = np.random.choice(len(tr_spk_idxs_[i_]), 2, replace=False).tolist()
                        for rand_i in rand_choice:
                            neg_pairs.append((neg_anchor, tr_spk_idxs_[i_][rand_i]))
                        # tr_spk_idxs_[i_].pop(rand_choice)
        else:
            print('skipping ', tr_spk_i)

    neg_pairs = list(set(neg_pairs))
    N_neg = len(neg_pairs)

    # create positive examples - maintain the mixing ratio depending on the number of utterances per speaker
    N_pos_ = N_neg # start with a 100K positive examples
    pos_pair_counts = [comb(len(i), 2) for i in tr_spk_idxs]
    print sum(pos_pair_counts)
    pos_pair_ratios = [np.round(N_pos_ * i / sum(pos_pair_counts)) for i in pos_pair_counts]
    print sum(pos_pair_ratios)
    pos_pairs_ = [random_combination(itertools.combinations(tr_spk_idxs[i], 2), pos_pair_ratios[i]) \
                  for i in range(len(pos_pair_ratios))]
    pos_pairs = list(set(itertools.chain(*pos_pairs_)))
    N_pos = len(pos_pairs)
    print N_pos, N_neg
    shuffle(pos_pairs)
    shuffle(neg_pairs)
    return pos_pairs, neg_pairs

def create_base_network(input_dim):
    seq = Sequential()
    seq.add(Dense(256, input_shape=(input_dim,), activation='relu'))
    # seq.add(Dropout(0.05))
    seq.add(Dense(512, activation='relu', W_constraint=maxnorm(1)))
    seq.add(Dropout(0.05))
    seq.add(Dense(256, activation='relu', W_constraint=maxnorm(1)))
    return seq

def euclidean_distance(vects):
    x, y = vects
    x = K.l2_normalize(x, axis=-1)
    y = K.l2_normalize(y, axis=-1)
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True))


def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

def cosine_distance(vests):
    x, y = vests
    x = K.l2_normalize(x, axis=-1)
    y = K.l2_normalize(y, axis=-1)
    return K.mean(x * y, axis=-1, keepdims=True)

def cos_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0],1)


def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    '''
    margin = 1
    return K.mean((y_true*0.5*K.square(y_pred)) + ((1 - y_true)*0.5*K.square(K.maximum(margin - y_pred, 0))))

def compute_accuracy(predictions, labels):
    '''Compute classification accuracy with a fixed threshold on distances.
    '''
    return labels[predictions.ravel() < 0.5].mean()

batch_size = 50
n_epochs = 50
nfilter = 5
subsample_rate = 3

# lets take the network trained with lots of data (librispeech - 1166 spk)
# and adapt it for WSJ (283 spk) with contrastive loss

# trained model file
model_file = 'voicenet_librispeech_1165.h5'

# with what file do I adapt  - with the new loss
# in lack of lots of data - you can use this to improve discriminability
# instead of retaining the whole LSTM net
feat_scp_file = './wsj/mfcc_rand.scp'

mfcc, spks, labels = scp_parser(feat_scp_file)

phase1_feats_file = './voicenet_librispeech_1165_wsj_mfcc_rand_256dim.npy'
if not os.path.isfile(phase1_feats_file):
    inp_dim = mfcc.shape[2]
    model = load_model(model_file)
    model.summary()

    first_dense_layer = K.function([model.layers[0].input, K.learning_phase()],
                                   [model.layers[-4].output])
    phase1_feats = first_dense_layer([mfcc, 0])[0]  # testing phase
else: phase1_feats = np.load(phase1_feats_file)

spk_counts = Counter(spks)
spk_ids = spk_counts.keys()

perc_spks_for_train = 0.8
# randomly pick 80% of speakers for train rest for test
pick_idxs = np.random.rand(len(spk_counts)) <= perc_spks_for_train

N_subj_tr = sum(pick_idxs)
N_subj_cv = sum(np.invert(pick_idxs))

tr_spks = np.array(spk_ids)[pick_idxs].tolist()
cv_spks = np.array(spk_ids)[np.invert(pick_idxs)].tolist()

# tr_spk_idxs = np.array([i for i in range(len(spks)) if spks[i] in tr_spks])
# cv_spk_idxs = np.array([i for i in range(len(spks)) if spks[i] in cv_spks])

tr_spk_idxs = [np.nonzero(np.array(spks) == tr_spk)[0].tolist() for tr_spk in tr_spks]
cv_spk_idxs = [np.nonzero(np.array(spks) == cv_spk)[0].tolist() for cv_spk in cv_spks]

# - - creating pos/neg for tr
tr_pos_pairs, tr_neg_pairs = create_pos_neg_pairs(tr_spk_idxs)
cv_pos_pairs, cv_neg_pairs = create_pos_neg_pairs(cv_spk_idxs)
shuffle(tr_pos_pairs)
shuffle(tr_neg_pairs)
shuffle(cv_pos_pairs)
shuffle(cv_neg_pairs)

N_tr_samples = min(len(tr_pos_pairs), len(tr_neg_pairs))
# alternate between posttiive and negatiove examples
tr_pos_neg_iter = [iter(tr_pos_pairs[:N_tr_samples]),iter(tr_neg_pairs[:N_tr_samples])]
tr_pos_neg_pairs = list(it.next() for it in \
                    itertools.cycle(tr_pos_neg_iter))
tr_pos_neg_data = np.array([phase1_feats[i,:] for i in tr_pos_neg_pairs])

tr_pos_y = np.ones(N_tr_samples).astype(int)
tr_neg_y = np.zeros(N_tr_samples).astype(int)
tr_y = np.array(list(it.next() for it in itertools.cycle([iter(tr_pos_y),iter(tr_neg_y)])))

N_cv_samples = min(len(cv_pos_pairs), len(cv_neg_pairs))
# alternate between posttiive and negatiove examples
cv_pos_neg_iter = [iter(cv_pos_pairs[:N_cv_samples]),iter(cv_neg_pairs[:N_cv_samples])]
cv_pos_neg_pairs = list(it.next() for it in \
                    itertools.cycle(cv_pos_neg_iter))
cv_pos_neg_data = np.array([phase1_feats[i,:] for i in cv_pos_neg_pairs])

cv_pos_y = np.ones(N_cv_samples).astype(int)
cv_neg_y = np.zeros(N_cv_samples).astype(int)
cv_y = np.array(list(it.next() for it in itertools.cycle([iter(cv_pos_y),iter(cv_neg_y)])))

phase1_dim = phase1_feats.shape[1]
base_network = create_base_network(phase1_dim)

input_a = Input(shape=(phase1_dim,))
input_b = Input(shape=(phase1_dim,))

processed_a = base_network(input_a)
processed_b = base_network(input_b)

distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)([processed_a, processed_b])
model_phase2 = Model(input=[input_a, input_b], output=distance)

# output_cos = Lambda(cosine_distance, output_shape=cos_dist_output_shape)([processed_a, processed_b])
# model_phase2 = Model(input=[input_a, input_b], output=output_cos)


# train
rms = RMSprop()
nadam = Nadam()
sgd = SGD(momentum=0.2 ,nesterov=True)
nb_epoch = 40
model_phase2.compile(loss=contrastive_loss, optimizer=sgd)
model_phase2.fit([tr_pos_neg_data[:,0], tr_pos_neg_data[:,1]], tr_y,
          validation_data=([cv_pos_neg_data[:,0], cv_pos_neg_data[:,1]], cv_y),
          batch_size=200,
          nb_epoch=nb_epoch)

tr_pred = model_phase2.predict([tr_pos_neg_data[:, 0], tr_pos_neg_data[:, 1]])
tr_acc = compute_accuracy(tr_pred, tr_y)
cv_pred = model_phase2.predict([cv_pos_neg_data[:, 0], cv_pos_neg_data[:, 1]])
te_acc = compute_accuracy(cv_pred, cv_y)

print('* Accuracy on training set: %0.2f%%' % (100 * tr_acc))
print('* Accuracy on test set: %0.2f%%' % (100 * te_acc))