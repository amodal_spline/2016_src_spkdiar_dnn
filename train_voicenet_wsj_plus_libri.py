from kaldi_io import read_mat_scp
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
#from keras.layers import Masking, TimeDistributed
from keras.layers.recurrent import LSTM

from keras.layers.convolutional import AtrousConvolution1D
from keras.layers.pooling import MaxPooling1D


import sys
import numpy as np

def scp_parser(feat_scp_file, maxlen=250, speaker_id_pick=3):
    # speake
    # maxlen = 250  # 5 seconds length frame shift 20ms
    spks_ = []
    mfcc = []

    for key, mat in read_mat_scp(open(feat_scp_file, 'r')):
        if mat.shape[0] < maxlen:
            mat = np.pad(mat, ((0, maxlen - mat.shape[0]), (0, 0)), 'constant', constant_values=0)
        else:
            l = (mat.shape[0] - maxlen) / 2
            mat = mat[l:l + maxlen, :]

        mfcc.append(mat)
        spks_.append(key.split('-')[0])

    mfcc = np.array(mfcc)

    spks = spks_ #[i[:speaker_id_pick] for i in spks_]
    nb_classes = len(set(spks))
    _, spks_1hot = np.unique(spks, return_inverse=True)
    labels = np.eye(nb_classes)[spks_1hot]
    return mfcc, spks, labels


#Load the feature file with labels
feat_scp_file=sys.argv[1]
maxlen=250	#5 seconds length frame shift 20ms
batch_size=50
n_epochs=50
nfilter=5
subsample_rate=3
spks=[]
mfcc=[]


#mfcc, spks, labels = scp_parser(feat_scp_file)
X = np.load('wsj_plus_librispeech.npz')
mfcc = X['mfcc']
spks = X['spks']
labels = X['labels']

inp_dim=mfcc.shape[2]
nb_classes=len(set(spks))
print "num classes =",nb_classes
print mfcc.shape, len(spks)

#setup network architecture

model = Sequential()

model.add(AtrousConvolution1D(64,nfilter,subsample_length=subsample_rate,input_shape=(maxlen,inp_dim)))
model.add(Activation('relu'))
model.add(AtrousConvolution1D(64,nfilter,subsample_length=subsample_rate))
model.add(Activation('relu'))
model.add(MaxPooling1D(pool_length=subsample_rate))
model.add(Dropout(0.25))


model.add(LSTM(128,return_sequences=True))
model.add(Dropout(0.25))
model.add(LSTM(128))
model.add(Dropout(0.25))
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes))
model.add(Activation('softmax'))


model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
model.fit(mfcc,labels, batch_size=batch_size, nb_epoch=n_epochs,validation_split=0.2)
model.save('voicenet_wsj_and_librispeech_1449_spks_05_10_17.h5')
